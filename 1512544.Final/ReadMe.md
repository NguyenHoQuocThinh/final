﻿# Đại học khoa học tự nhiên
# -------------------------------------------------
###Họ tên: NGUYỄN HỒ QUỐC THỊNH
###MSSV 1512544
# -------------------------------------------------
#Chức năng đã hoàn thành:
1. Tạo 1 tag mới
2. Xóa 1 tag
3. Sửa tên 1 tag
4. Tạo 1 node mới vào trong 1 hoặc nhiều tag
5. Xóa 1 node
6. Xem biểu đồ
7. Thêm biểu tượng vào notification
8. Có thể view note, view statistic, exit từ icon ở notification
9. Tìm kiếm theo tên của note
#Luồng sự kiện chính:
* Người dùng khởi động chương trình và bắt đầu sử dụng
* Nhập tên một hoặc nhiều tag vào trong thanh tag name và nhập nội dung của note sau đó click nút ADD ở mục note để add 1 note mới
* Người dùng chọn chức năng Statistic để xem biểu đồ thống kê các note
* Muốn tạo 1 tag mới: Người dùng chọn chức năng ADD TAG sau đó nhập tên tag và click OK
* Muốn sửa tên 1 tag: Người dùng chọn tag trong List View tag và chọn tag cần sửa tên, chọn chức năng RENAME TAG và nhập tên mới sao cho không trùng với tên đã có và click OK
* Muốn xóa 1 tag: Người dùng chọn tag trong List View tag và chọn tag cần xóa, chọn chức năng DELETE TAG
* Muốn xóa 1 note: Người dùng chọn 1 note trong List View Note và chọn note cần xóa, chọn chức năng DELETE
* Muốn tìm kiếm note: Người dùng nhập tên note cần tìm vào trong khung tìm kiếm và kết quả sẽ được tự động trả về

#Luồng sự kiện phụ:
* Tạo 1 note thất bại: Người dùng nhập sai tên tag hoặc không có nội dung của note
* Sửa tên tag thất bại: Tên người dùng vừa nhập trùng với tên đã có
* Xóa 1 tag thất bại: Người dùng không chọn tag cần xóa

#Link bitbucket: https://NguyenHoQuocThinh@bitbucket.org/NguyenHoQuocThinh/final
#Link video demo hướng dẫn: https://youtu.be/UDyWxcTtmA8
