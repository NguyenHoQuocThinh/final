﻿// Final1.0.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Final1.0.h"
#include <windowsX.h>
#include <winuser.h>
#include <fstream>
#include <locale>
#include <WindowsX.h>
#include <codecvt>
#include <iostream>
#include <string>
#include <vector>
#include <shellapi.h>
#include <commctrl.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

#include <Objbase.h>
#pragma comment(lib, "Ole32.lib")
#include "RibbonFramework.h"
#include "RibbonIDs.h"

using namespace std;
#define MAX_LOADSTRING	100
#define MAX_LENGTH		500

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr)) {
		return FALSE;
	}
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_FINAL10, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FINAL10));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
	CoUninitialize();
    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = 0;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FINAL10));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_FINAL10);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      350, 200, 1100, 650, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

// LIST VARIALBLE
//==================================================================
#define FILE_PATH		L"data.txt"

#define WM_MYMESSAGE (WM_USER+ 1)

UINT EXIT = 100;
UINT VIEWNOTE = 200;
UINT VIEWSTATISTIC = 300;

NOTIFYICONDATA notifyIcon;

HHOOK hHook = NULL;

HINSTANCE hinstLib;

HWND g_hWnd;
HWND edit_Search; // edit box:  thanh tìm kiếm
HWND btn_Search; // button: tìm kiếm
HWND edit_Tags; // edit box: thêm note vào các tag
HWND edit_NoteName; // edit box: tên của note
HWND edit_NoteContent; // edit box: nội dung của note
HWND btn_AddNote; // button: thêm note
HWND btn_DeteleNote; // button: xóa note
HWND btn_CancelNote; // button: hủy note vừa gõ
HWND listView_Tags; // list view: danh sách các tag
HWND listView_NoteName; // list view: danh sách các note trong tag
HWND listView_ShowNotes;

HWND dlg_Edit_TagName;
HWND dlg_Edit_TagRename;

bool WindowIsClose = false;
bool isHiding = false;

int listTagCount = 0;	// Đếm các tag;
int listNoteNameCount = 0; // đếm các note
int listShowNoteCount = 0;

int tagItemSelect = -1;
int noteItemSelect = -1;

WCHAR TagCurrentSelected[1024];

LPARAM lParam;
struct Note {
	WCHAR TagName[20];
	WCHAR NoteName[1024];
	WCHAR NoteContent[10240];
};

struct Tag {
	WCHAR TagName[20];
};

vector<Note*> NotesData; // Lưu các note
vector<Note*> ListNotes; // thể hiện các note vào list view
vector<Tag*> ListTags;

vector<COLORREF> ListColor;
//==================================================================

// LIST FUCTION
//==================================================================
LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm);

INT_PTR CALLBACK AddTag(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK RenameTag(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ViewStatistic(HWND hDlg, UINT mewssage, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK MyHookProc(int nCode, WPARAM wParam, LPARAM lParam);

HWND CreateListViewOfTag(HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight);
HWND CreateListViewOfNoteName(HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight);
HWND CreateListViewOfShowNotes(HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight);
void AddBasicTags(HWND hWnd, int &listTagCount);
void AddNewNote();
void SplitNoteTags(WCHAR NoteTagName[1024]);
bool CheckNoteTagAndTag(WCHAR NoteTagName[1024]);
void SaveData(wstring path);
void LoadData(wstring path);
void SearchList(WCHAR key[1024]);
void ShowListAfterSearch();
int TotalNotes();
void sort(int *a, int n);
void CreateColor();
void AddNotificationIcon(HWND hWnd);
void doRemoveHook(HWND hWnd);
void doInstallHook(HWND hWnd);
//==================================================================
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	g_hWnd = hWnd;

	bool initSuccess;
    switch (message)
    {
	HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);
	case WM_CREATE: {
		doInstallHook(hWnd);
		CreateColor();
		initSuccess = InitializeFramework(hWnd);
		if (!initSuccess) {
			return -1;
		}

		INITCOMMONCONTROLSEX icc;
		icc.dwSize = sizeof(icc);
		icc.dwICC = ICC_WIN95_CLASSES;
		InitCommonControlsEx(&icc);

		// Get system font
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		HFONT hFont = CreateFont(lf.lfHeight - 6, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);

		// Load data
		LoadData(FILE_PATH);

		//// Search edit box
		edit_Search = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, 10, 200, 280, 30, hWnd, NULL, hInst, NULL);
		SendMessage(edit_Search, WM_SETFONT, WPARAM(hFont), NULL);
		//
		////// Search button
		//btn_Search = CreateWindow(L"BUTTON", L"Tìm kiếm", WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 400, 160, 100, 30, hWnd, (HMENU)ID_BTN_SEARCH, hInst, NULL);
		//SendMessage(btn_Search, WM_SETFONT, WPARAM(hFont), NULL);
		//
		//// Note into tag(s) edit box
		edit_Tags = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, 630, 160, 400, 30, hWnd, NULL, hInst, NULL);
		SendMessage(edit_Tags, WM_SETFONT, WPARAM(hFont), NULL);
		//
		//// Note name edit box
		edit_NoteName = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL | WS_DISABLED, 630, 200, 400, 30, hWnd, NULL, hInst, NULL);
		SendMessage(edit_NoteName, WM_SETFONT, WPARAM(hFont), NULL);

		//// Note content edit box
		edit_NoteContent = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL | ES_MULTILINE, 510, 240, 520, 300, hWnd, NULL, hInst, NULL);
		SendMessage(edit_NoteContent, WM_SETFONT, WPARAM(hFont), NULL);

		////// Add note button
		//btn_AddNote = CreateWindow(L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 510, 550, 255, 50, hWnd, (HMENU)ID_BTN_ADDNOTE, hInst, NULL);
		//SendMessage(btn_AddNote, WM_SETFONT, WPARAM(hFont), NULL);

		//// Cancel button
		//btn_CancelNote = CreateWindow(L"BUTTON", L"Hủy", WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 775, 550, 255, 50, hWnd, (HMENU)ID_BTN_ADDNOTE, hInst, NULL);
		//SendMessage(btn_CancelNote, WM_SETFONT, WPARAM(hFont), NULL);

		// Tag ListView
		listView_Tags = CreateListViewOfTag(hWnd, ID_LV_TAGS, hInst, 300, 200, 200, 150);
		SendMessage(listView_Tags, WM_SETFONT, WPARAM(hFont), NULL);
		AddBasicTags(hWnd, listTagCount);
		// NoteName ListView
		listView_NoteName = CreateListViewOfNoteName(hWnd, ID_LV_NOTENAME, hInst, 300, 360, 200, 180);
		SendMessage(listView_NoteName, WM_SETFONT, WPARAM(hFont), NULL);
		// ShowNotes ListView
		listView_ShowNotes = CreateListViewOfShowNotes(hWnd, ID_LV_SHOWNOTES, hInst, 10, 240, 280, 300);

		////// Delete note button
		//btn_DeteleNote = CreateWindow(L"BUTTON", L"Xóa", WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON, 220, 550, 280, 50, hWnd, (HMENU)ID_BTN_ADDNOTE, hInst, NULL);
		//SendMessage(btn_DeteleNote, WM_SETFONT, WPARAM(hFont), NULL);

		// STATIC content
		HWND other;
		other = CreateWindow(L"STATIC", L"Thêm vào tag", WS_CHILD | WS_VISIBLE | SS_LEFT, 510, 160, 110, 30, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), NULL);
		other = CreateWindow(L"STATIC", L"Tên note", WS_CHILD | WS_VISIBLE | SS_LEFT, 510, 200, 110, 30, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), NULL);
		other = CreateWindow(L"STATIC", L"Tìm kiếm", WS_CHILD | WS_VISIBLE | SS_LEFT, 10, 170, 100, 30, hWnd, NULL, hInst, NULL);
		SendMessage(other, WM_SETFONT, WPARAM(hFont), NULL);
		AddNotificationIcon(hWnd);

	}break;
	case WM_KEYDOWN: {
		doInstallHook(hWnd);
	}break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
			int wmEvent = HIWORD(wParam);
            // Parse the menu selections:
			if ((HWND)lParam == edit_Search) {
				if (wmEvent == EN_CHANGE) {
					WCHAR key[1024];
					GetWindowText(edit_Search, key, 1024);
					SearchList(key);
					ShowListAfterSearch();
				}
			}

            switch (wmId)
            {
			case ID_BTN_EXIT: {
				DestroyWindow(hWnd);
			}break;
			case ID_BTN_CLEARNOTE: {
				SetWindowText(edit_Tags, L"");
				SetWindowText(edit_NoteContent, L"");
				SetWindowText(edit_NoteName, L"");
			}break;
			case ID_BTN_ADDTAG: {
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ADDTAG), hWnd, AddTag);
			}break;
			case ID_BTN_DELETENOTE: {
				if (noteItemSelect!=-1) {
					
					WCHAR *tag;
					WCHAR *notename;
					WCHAR *notecontent;
					int len;
					
					len = GetWindowTextLength(edit_Tags);
					tag = new WCHAR[len + 1];
					GetWindowText(edit_Tags, tag, len + 1);

					len = GetWindowTextLength(edit_NoteName);
					notename = new WCHAR[len + 1];
					GetWindowText(edit_NoteName, notename, len + 1);

					len = GetWindowTextLength(edit_NoteContent);
					notecontent = new WCHAR[len + 1];
					GetWindowText(edit_NoteContent, notecontent, len + 1);
					
					if (wcscmp(tag, L"") == 0 || wcscmp(notename, L"") == 0 || wcscmp(notecontent, L"") == 0) {
						MessageBox(0, L"Try again", L"Warning", 0);
						break;
					}

					int pos = -1;

					for (int i = 0; i < NotesData.size(); i++) {
						if (wcscmp(NotesData[i]->TagName, tag) == 0 && wcscmp(NotesData[i]->NoteName, notename) == 0 && wcscmp(NotesData[i]->NoteContent, notecontent) == 0) {
							pos = i;
						}
					}

					NotesData.erase(NotesData.begin() + pos);

					ListView_DeleteItem(listView_NoteName, noteItemSelect);
					noteItemSelect = -1;
					SetWindowText(edit_Tags, L"");
					SetWindowText(edit_NoteContent, L"");
					SetWindowText(edit_NoteName, L"");
				}
				else {
					MessageBox(0, L"You can't use this function", L"Warning", 0);
				}
			}break;
			case ID_BTN_DELETETAG: {
				if (tagItemSelect != -1) {
					ListTags.erase(ListTags.begin() + tagItemSelect);
					WCHAR tagName[1024];
					ListView_GetItemText(listView_Tags, tagItemSelect, 0, tagName, 1024);
					for (int i = 0; i < NotesData.size(); i++) {
						if (wcscmp(tagName, NotesData[i]->TagName) == 0) {
							NotesData.erase(NotesData.begin() + i);
							i--;
						}

					}
					if (wcscmp(TagCurrentSelected, tagName) == 0) {
						listNoteNameCount = 0;
						ListView_DeleteAllItems(listView_NoteName);
					}
					ListView_DeleteItem(listView_Tags, tagItemSelect);
					SetWindowText(edit_Tags, L"");
					SetWindowText(edit_NoteContent, L"");
					SetWindowText(edit_NoteName, L"");
					tagItemSelect = -1;
					listTagCount--;
					MessageBox(0, L"Delete tag successful", L"", 0);
				}
				else {
					MessageBox(0, L"You can't use this function", L"Warning", 0);
				}
			}break;
			case ID_BTN_RENAMETAG: {
				if (tagItemSelect != -1) {
					DialogBox(hInst, MAKEINTRESOURCE(IDD_RENAMETAG), hWnd, RenameTag);
					tagItemSelect = -1;
				}
				else {
					MessageBox(0, L"You can't use this function", L"Warning", 0);
				}
			}break;
			case ID_BTN_ADDNOTE: {
				AddNewNote();
			}break;
			case ID_BTN_VIEWSTATISTIC: {
				DialogBox(hInst, MAKEINTRESOURCE(IDD_VIEWSTATISTIC), hWnd, ViewStatistic);
				
			}break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
			RECT x;
			GetClientRect(hWnd, &x);
			int dlgWidth = x.right - x.left;
			int dlgHeight = x.bottom - x.top;
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);

            EndPaint(hWnd, &ps);
        }
        break;
	case WM_MYMESSAGE: {
		switch (lParam) {
		case WM_RBUTTONDOWN: {
			MENUITEMINFO separatorBtn = { 0 };
			separatorBtn.cbSize = sizeof(MENUITEMINFO);
			separatorBtn.fMask = MIIM_FTYPE;
			separatorBtn.fType = MFT_SEPARATOR;
			
			HMENU hMenu = CreatePopupMenu();
			if (hMenu) {
				InsertMenu(hMenu, -1, MF_BYPOSITION, VIEWNOTE, L"View Note");
				InsertMenuItem(hMenu, -1, false, &separatorBtn);
				InsertMenu(hMenu, -1, MF_BYPOSITION, VIEWSTATISTIC, L"View Statistic");
				InsertMenuItem(hMenu, -1, false, &separatorBtn);
				InsertMenu(hMenu, -1, MF_BYPOSITION, EXIT, L"Exit");
				InsertMenuItem(hMenu, -1, false, &separatorBtn);
				POINT pt;
				GetCursorPos(&pt);
				SetForegroundWindow(hWnd);
				UINT clicked = TrackPopupMenu(hMenu, TPM_RIGHTBUTTON | TPM_NONOTIFY | TPM_RETURNCMD, pt.x, pt.y, 0, hWnd, NULL);
				if (clicked == VIEWSTATISTIC) {
					DialogBox(hInst, MAKEINTRESOURCE(IDD_VIEWSTATISTIC), hWnd, ViewStatistic);
				}
				else if (clicked == EXIT) {
					DestroyWindow(hWnd);
				}
				else if (clicked == VIEWNOTE) {
					if (WindowIsClose == true) {
						ShowWindow(hWnd, SW_SHOW);
					}
				}
				PostMessage(hWnd, WM_NULL, 0, 0);
				DestroyMenu(hMenu);
			}
		}break;
		}
	}break;
	case WM_CLOSE: {
		WindowIsClose = ShowWindow(hWnd, SW_HIDE);
	}break;
    case WM_DESTROY:
		SaveData(FILE_PATH);
		DestroyFramework();
		doRemoveHook(hWnd);
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm) {
	int nCurSelIndex;

	LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)pnm;


	switch (idFrom) {
	case ID_LV_TAGS: {
		if (pnm->code == NM_CLICK) {
			SetWindowText(edit_Tags, L"");
			SetWindowText(edit_NoteContent, L"");
			SetWindowText(edit_NoteName, L"");
			tagItemSelect = ListView_GetNextItem(listView_Tags, -1, LVNI_SELECTED);
			if (tagItemSelect >= 0) {
				ListView_GetItemText(listView_Tags, tagItemSelect, 0, TagCurrentSelected, 1024);
				ListView_DeleteAllItems(listView_NoteName);
				listNoteNameCount = 0;
				for (int i = 0; i < NotesData.size(); i++) {
					if (wcscmp(ListTags[tagItemSelect]->TagName, NotesData[i]->TagName) == 0){
						if (listNoteNameCount == 0) {
							SetWindowText(edit_Tags, NotesData[i]->TagName);
							SetWindowText(edit_NoteName, NotesData[i]->NoteName);
							SetWindowText(edit_NoteContent, NotesData[i]->NoteContent);
						}
						LV_ITEM lv;
						lv.mask = LVIF_TEXT | LVIF_PARAM;
						lv.iItem = listNoteNameCount;
						lv.iSubItem = 0;
						lv.pszText = NotesData[i]->NoteName;
						ListView_InsertItem(listView_NoteName, &lv);
						listNoteNameCount++;
						
					}
				}


			}
		}
	}break;
	case ID_LV_NOTENAME: {
		if (pnm->code == NM_CLICK) {
			int pos = ListView_GetNextItem(listView_NoteName, -1, LVNI_SELECTED);
			WCHAR buffer[1024];
			noteItemSelect = pos;
			ListView_GetItemText(listView_NoteName, pos, 0, buffer, 1024);
			if(pos>=0){
				int index = -1;
				for (int i = 0; i < NotesData.size(); i++) {
					if (wcscmp(buffer, NotesData[i]->NoteName) == 0 && wcscmp(NotesData[i]->TagName, TagCurrentSelected)==0) {
						index = i;
						break;
					}
				}
				SetWindowText(edit_Tags, NotesData[index]->TagName);
				SetWindowText(edit_NoteName, NotesData[index]->NoteName);
				SetWindowText(edit_NoteContent, NotesData[index]->NoteContent);
			}
		}
	}break;
	case ID_LV_SHOWNOTES: {
		if(pnm->code == NM_CLICK) {
			int pos = ListView_GetNextItem(listView_ShowNotes, -1, LVNI_SELECTED);
			WCHAR buffer_Tag[1024], buffer_NoteName[1024];
			ListView_GetItemText(listView_ShowNotes, pos, 0, buffer_Tag, 1024);
			ListView_GetItemText(listView_ShowNotes, pos, 1, buffer_NoteName, 1024);
			if (pos >= 0) {
				int index = -1;
				for (int i = 0; i < ListNotes.size(); i++) {
					if (wcscmp(buffer_Tag, ListNotes[i]->TagName) == 0 && wcscmp(buffer_NoteName, ListNotes[i]->NoteName) == 0) {
						index = i;
						break;
					}
				}
				SetWindowText(edit_Tags, ListNotes[index]->TagName);
				SetWindowText(edit_NoteName, ListNotes[index]->NoteName);
				SetWindowText(edit_NoteContent, ListNotes[index]->NoteContent);
			}
		}
	}break;
	default:
		break;
	}
	return 0;
}

INT_PTR CALLBACK AddTag(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);

	HWND other = GetDlgItem(hDlg, IDC_NEWTAG);
	dlg_Edit_TagName = GetDlgItem(hDlg, IDC_ADDTAG1);
	switch (message) {

	case WM_INITDIALOG: {
		SetWindowText(hDlg, L"Add New Tag");
		SetWindowText(other, L"Tag Name");
		SetWindowText(dlg_Edit_TagName, L"");
		return (INT_PTR)TRUE;
	}

	case WM_COMMAND: {
		int id = LOWORD(wParam);
		switch (id) {

		case IDCANCEL: {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}

		case IDOK: {
			int length;
			WCHAR* buffer;
			length = GetWindowTextLength(dlg_Edit_TagName);
			buffer = new WCHAR[length + 1];
			GetWindowText(dlg_Edit_TagName, buffer, length + 1);
		

			LV_ITEM lv;
			lv.mask = LVIF_TEXT | LVIF_PARAM;

			lv.iItem = listTagCount;
			lv.iSubItem = 0;
			lv.pszText = buffer;
			ListView_InsertItem(listView_Tags, &lv);
			Tag* x = new Tag();
			ListView_GetItemText(listView_Tags, listTagCount, 0, x->TagName, 100);
			ListTags.push_back(x);
			listTagCount++;
			

			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		}
		break;	
	}

	case WM_CLOSE: {
		EndDialog(hDlg, LOWORD(wParam));
		return(INT_PTR)TRUE;
	}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK RenameTag(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	HWND other = GetDlgItem(hDlg, IDC_STATIC_RENAMETAG);
	dlg_Edit_TagRename = GetDlgItem(hDlg, IDC_EDIT_RENAMETAG);
	switch (message) {
	case WM_INITDIALOG: {
		SetWindowText(hDlg, L"Rename Tag");
		SetWindowText(other, L"New name");
		SetWindowText(dlg_Edit_TagRename, L"");
	}break;
	case WM_COMMAND: {
		int id = LOWORD(wParam);
		switch (id) {
		case IDOK: {
			int length;
			WCHAR* buffer;
			length = GetWindowTextLength(dlg_Edit_TagRename);
			buffer = new WCHAR[length + 1];
			GetWindowText(dlg_Edit_TagRename, buffer, length + 1);

			for (int i = 0; i < ListTags.size(); i++) {
				if (wcscmp(ListTags[i]->TagName, buffer) == 0) {
					MessageBox(0, L"Name is already exist", L"Warning", 0);
					EndDialog(hDlg, LOWORD(wParam));
					return (INT_PTR)TRUE;
				}
			}

			WCHAR *tag = new WCHAR[100];
			ListView_GetItemText(listView_Tags, tagItemSelect, 0, tag, 100);

			for (int i = 0; i < ListTags.size(); i++) {
				if (wcscmp(ListTags[i]->TagName, tag) == 0) {
					wcscpy_s(ListTags[i]->TagName, L"");
					wcscpy_s(ListTags[i]->TagName, buffer);
				}
			}

			for (int i = 0; i < NotesData.size(); i++) {
				if (wcscmp(NotesData[i]->TagName, tag) == 0) {
					wcscpy_s(NotesData[i]->TagName, L"");
					wcscpy_s(NotesData[i]->TagName, buffer);
				}
			}

			ListView_SetItemText(listView_Tags, tagItemSelect, 0, buffer);



			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}break;
		case IDCANCEL: {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}break;
		}
	}break;
	case WM_CLOSE: {
		EndDialog(hDlg, LOWORD(wParam));
		return(INT_PTR)TRUE;
	}break;
	}
	return (INT_PTR)FALSE;
}


INT_PTR CALLBACK ViewStatistic(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rcClient;

	GetClientRect(hDlg, &rcClient);
	int dlgWidth = rcClient.right - rcClient.left;
	int dlgHeight = rcClient.bottom - rcClient.top;
	float *ratio = new float[ListTags.size()];
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight - 6, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	
	

	int mode = -1;

	int *numberOfTag = new int[ListTags.size()];
	for (int i = 0; i < ListTags.size(); i++) {
		int dem = 0;
		for (int j = 0; j < NotesData.size(); j++) {
			if (wcscmp(NotesData[j]->TagName, ListTags[i]->TagName) == 0) {
				dem++;
			}
		}
		numberOfTag[i] = dem;
	}

	int TotalNote = TotalNotes();

	if (TotalNote != 0) {
		for (int i = 0; i < ListTags.size(); i++) {
			float per = (float)numberOfTag[i] / (float)TotalNote;
			ratio[i] = per;
		}
	}
	else {
		for (int i = 0; i < ListTags.size(); i++) {
			float per = 0;
			ratio[i] = per;
		}
	}
	

	HWND *ra = new HWND[ListTags.size()];
	switch (message) {
	case WM_INITDIALOG: {

		
		int left = 300, top = 50;
		
		HWND other;
		
		for (int i = 0; i < ListTags.size(); i++) {
			other = CreateWindow(L"STATIC", ListTags[i]->TagName, WS_CHILD | WS_VISIBLE | SS_LEFT, left, top, 100, 30, hDlg, NULL, hInst, NULL);
			SendMessage(other, WM_SETFONT, WPARAM(hFont), NULL);
			top += 50;
		}

		

		SetWindowText(hDlg, L"View Statistic");
	}break;
	case WM_PAINT: {
		hdc = BeginPaint(hDlg, &ps);
		RECT *rect = new RECT;
		int y_standard = 50;

		
		int size = dlgHeight - 100;

		int y_title = 65;
		int y_ratio = 40;
		int k = 0;
		for (int i = 0; i < ListTags.size(); i++) {
			if (ratio[i] > 0){
				int x = ratio[i] * size;
				HPEN hPen = CreatePen(PS_SOLID, 4, ListColor[k]);
				SelectObject(hdc, hPen);
				Rectangle(hdc, 50, y_standard, 100, y_standard + x);
				
				rect->left = 50;
				rect->top = y_standard;
				rect->right = 100;
				rect->bottom = y_standard + x;
				HBRUSH brush = CreateSolidBrush(ListColor[k]);
				FillRect(hdc, rect, brush);

				
				int midle = (rect->bottom - rect->top)/2;
				MoveToEx(hdc, 100, y_standard + midle, NULL);
				LineTo(hdc, 250, y_title);
				MoveToEx(hdc, 250, y_title, NULL);
				LineTo(hdc, 290, y_title);
				

				WCHAR buffer[20];
				unsigned long long a = (int)(ratio[i] * 100);
				wsprintf(buffer, L"%I64d%", a);
				wcscat_s(buffer, L"%");
				HWND other = CreateWindow(L"STATIC", buffer, WS_CHILD | WS_VISIBLE | SS_LEFT, 255, y_ratio, 35, 19, hDlg, NULL, hInst, NULL);
				SendMessage(other, WM_SETFONT, WPARAM(hFont), NULL);
				y_ratio += 50;

				y_title += 50;
				k++;
				y_standard += x;
			}
			else {
				HPEN hPen = CreatePen(PS_SOLID, 4, RGB(76, 0, 153));
				SelectObject(hdc, hPen);
				MoveToEx(hdc, 250, y_title, NULL);
				LineTo(hdc, 290, y_title);

				WCHAR buffer[20];
				unsigned long long a = (int)(ratio[i] * 100);
				wsprintf(buffer, L"%I64d%", a);
				wcscat_s(buffer, L"%");
				HWND other = CreateWindow(L"STATIC", buffer, WS_CHILD | WS_VISIBLE | SS_LEFT, 255, y_ratio, 35, 19, hDlg, NULL, hInst, NULL);
				SendMessage(other, WM_SETFONT, WPARAM(hFont), NULL);

				y_ratio += 50;
				y_title += 50;
			}
		}
		EndPaint(hDlg, &ps);
	}break;
	case WM_COMMAND: {
		int id = LOWORD(wParam);
		switch (id) {
		case IDOK: {
			EndDialog(hDlg, TRUE);
			return (INT_PTR)TRUE;
		}break;
		case IDCANCEL: {
			EndDialog(hDlg, FALSE);
			return (INT_PTR)TRUE;
		}break;
		}
	}break;
	case WM_CLOSE: {
		EndDialog(hDlg, LOWORD(wParam));
		return (INT_PTR)FALSE;
	}
	}
	return (INT_PTR)FALSE;
}

HWND CreateListViewOfTag(HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight) {
	HWND m_listView = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER | WS_HSCROLL | WS_VSCROLL, x, y, nWidth, nHeight, parentWnd, (HMENU)ID, hParentInst, NULL);
	INITCOMMONCONTROLSEX icex;

	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	LVCOLUMN lvCol;
	lvCol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_CENTER;

	lvCol.cx = 200;
	lvCol.pszText = L"TAG";
	ListView_InsertColumn(m_listView, 0, &lvCol);

	return m_listView;
}

void AddBasicTags(HWND hWnd, int &listTagCount) {
	WCHAR tag1[20] = L"Học tập";
	WCHAR tag2[20] = L"Gia đình";
	WCHAR tag3[20] = L"Todo";

	LV_ITEM lv;
	lv.mask = LVIF_TEXT | LVIF_PARAM;

	for (int i = 0; i < ListTags.size(); i++) {
		lv.iItem = listTagCount;
		lv.iSubItem = 0;
		lv.pszText = ListTags[i]->TagName;
		ListView_InsertItem(listView_Tags, &lv);
		listTagCount++;
	}


	/*lv.iItem = listTagCount;
	lv.iSubItem = 0;
	lv.pszText = tag1;
	ListView_InsertItem(listView_Tags, &lv);
	Tag* x = new Tag();
	ListView_GetItemText(listView_Tags, listTagCount, 0, x->TagName, 100);
	ListTags.push_back(x);
	listTagCount++;
	

	lv.iItem = listTagCount;
	lv.iSubItem = 0;
	lv.pszText = tag2;
	ListView_InsertItem(listView_Tags, &lv);
	Tag* y = new Tag();
	ListView_GetItemText(listView_Tags, listTagCount, 0, y->TagName, 100);
	ListTags.push_back(y);
	listTagCount++;

	lv.iItem = listTagCount;
	lv.iSubItem = 0;
	lv.pszText = tag3;
	ListView_InsertItem(listView_Tags, &lv);
	Tag* z = new Tag();
	ListView_GetItemText(listView_Tags, listTagCount, 0, z->TagName, 100);
	ListTags.push_back(z);
	listTagCount++;*/

}

HWND CreateListViewOfNoteName(HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight) {
	HWND m_listView = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER | WS_HSCROLL, x, y, nWidth, nHeight, parentWnd, (HMENU)ID, hParentInst, NULL);
	INITCOMMONCONTROLSEX icex;

	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	LVCOLUMN lvCol;
	lvCol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_CENTER;

	lvCol.cx = 280;
	lvCol.pszText = L"NAME";
	ListView_InsertColumn(m_listView, 0, &lvCol);
	return m_listView;
}

HWND CreateListViewOfShowNotes(HWND parentWnd, long ID, HINSTANCE hParentInst, int x, int y, int nWidth, int nHeight) {
	HWND m_listView = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | LVS_REPORT | WS_BORDER | WS_HSCROLL, x, y, nWidth, nHeight, parentWnd, (HMENU)ID, hParentInst, NULL);
	
	INITCOMMONCONTROLSEX icex;
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES | ICC_TREEVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	LVCOLUMN lvCol;
	lvCol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_CENTER;

	lvCol.cx = 90;
	lvCol.pszText = L"TAG";
	ListView_InsertColumn(m_listView, 0, &lvCol);

	lvCol.cx = 190;
	lvCol.pszText = L"NAME";
	ListView_InsertColumn(m_listView, 1, &lvCol);

	return m_listView;
}


vector<WCHAR*> notetags;
void AddNewNote() {
	
	WCHAR tags[1024];
	int length;
	bool added = false;
	// GET NOTE TAG
	length = GetWindowTextLength(edit_Tags);
	GetWindowText(edit_Tags, tags, length + 1);
	if (wcscmp(tags, L"") == 0) {
		MessageBox(0, L"You can't use this function", L"Warning", 0);
		return;
	}
	SplitNoteTags(tags);
	for (int i = 0; i < notetags.size(); i++) {
		Note* x = new Note();
		wcscat_s(x->TagName, notetags[i]);
		if (CheckNoteTagAndTag(notetags[i]) == false) {
			WCHAR buffer[100];
			wsprintf(buffer, L"[%ls] is not a correct tag name", x->TagName);
			MessageBox(0, buffer, L"Error", 0);
		}
		else {
			// GET NOTE CONTENT
			length = GetWindowTextLength(edit_NoteContent);
			GetWindowText(edit_NoteContent, x->NoteContent, length + 1);
			if (length <= 0) {
				SetWindowText(edit_NoteContent, L"");
				MessageBox(0, L"Note content error", L"Error", 0);
				return;
			}
			WCHAR buffer[1024];
			int i = 0;
			if (wcslen(x->NoteContent) < 5) {
				for (; i < 5; i++) {
					if (x->NoteContent[i] == '\r')
						break;
					buffer[i] = x->NoteContent[i];
				}
			}
			else {
				for (; i < 10; i++) {
					if (x->NoteContent[i] == '\r')
						break;
					buffer[i] = x->NoteContent[i];
				}
			}
			buffer[i++] = '.';
			buffer[i++] = '.';
			buffer[i++] = '.';
			buffer[i] = '\0';
			
			wcscpy_s(x->NoteName, L"");
			wcscpy_s(x->NoteName, buffer);
			

			added = true;
			if (added == true) {
				ListNotes.push_back(x);
				NotesData.push_back(x);
			}	
		}
	}
	notetags.clear();
	if (added == true) {
		MessageBox(0, L"Add success", L"", 0);
		SetWindowText(edit_Tags, L"");
		SetWindowText(edit_NoteContent, L"");
		SetWindowText(edit_NoteName, L"");
	}
	else {
		MessageBox(0, L"Add error", L"Warning", 0);
	}
	
	
}

void SplitNoteTags(WCHAR NoteTagName[1024]) {
	int count = 0;
	for (int i = 0; i < wcslen(NoteTagName) - 1; i++) {
		if (NoteTagName[i] == ',')
			count++;
	}
	int k = 0;
	for (int i = 0; i < count+1; i++) {
		WCHAR* x = new WCHAR[50];
		int index = 0;
		bool firstSpace = false;
		while(NoteTagName[k] != ','){
			if (NoteTagName[k] == ' ' && firstSpace == false) {
				k++;
				firstSpace = true;
			}
			else {
				if (NoteTagName[k] == '\0')
					break;
				firstSpace = true;
				x[index++] = NoteTagName[k++];
			}
		}
		k++;
		x[index] = '\0';
		notetags.push_back(x);
	}
}

bool CheckNoteTagAndTag(WCHAR NoteTagName[1024]) {
	for (int i = 0; i < ListTags.size(); i++) {
		if (wcscmp(NoteTagName, ListTags[i]->TagName) == 0)
			return true;
	}
	return false;
}

void SaveData(wstring path) {
	const locale utf8_locale = locale(locale(), new codecvt_utf8<wchar_t>());
	std::wofstream f(path);
	f.imbue(utf8_locale);
	int note_index = 1;
	f << ListTags.size() << endl;
	for (int i = 0; i < ListTags.size(); i++) {
		f << wstring(ListTags[i]->TagName) << endl;
	}

	for (int i = 0; i < NotesData.size(); i++) {
		f << note_index << endl;
		f << wstring(NotesData[i]->TagName) << endl;
		f << wstring(NotesData[i]->NoteName) << endl;
		f << wstring(NotesData[i]->NoteContent) << endl;
		note_index++;
	}
}

void LoadData(wstring path) {
	const locale utf8_locale = locale(locale(), new codecvt_utf8<wchar_t >());
	wfstream f;
	f.imbue(utf8_locale);
	f.open(path, ios::in);
	wstring buffer;
	int note_index;
	ListNotes.clear();
	NotesData.clear();
	ListTags.clear();
	if (f.is_open()) {
		int numberOfTags;
		getline(f, buffer);
		numberOfTags = _wtoi64(buffer.c_str());
		for (int i = 0; i < numberOfTags; i++) {
			Tag* a = new Tag();
			getline(f, buffer);
			wcscpy_s(a->TagName, buffer.c_str());
			ListTags.push_back(a);
		}
		while (!f.eof()) {
			Note* x = new Note();
			getline(f, buffer);
			note_index = _wtoi64(buffer.c_str());
			if (note_index == 0)
				break;
			getline(f, buffer);
			wcscpy_s(x->TagName, buffer.c_str());
			if (wcslen(x->TagName) == 0)
				break;
			getline(f, buffer);
			wcscpy_s(x->NoteName, buffer.c_str());
			
			int z = 0;
			do {

				getline(f, buffer);
				if(buffer[buffer.length()-1]=='\r')
					buffer.erase(buffer.length() - 1);
				wcscat_s(x->NoteContent, buffer.c_str());
				int a = f.tellp();
				getline(f, buffer);
				int b = f.tellp();
				if (buffer.length() == 1 || f.eof() == true) {
					f.seekg(-(b - a), ios::cur);
					break;
				}
				else {
					wcscat_s(x->NoteContent, L"\r\n");

					f.seekg(-(b - a), ios::cur);
					
				}
			} while (true);
			ListNotes.push_back(x);
			NotesData.push_back(x);
			//getline(f, buffer);
		}
	}
	f.close();
}

void SearchList(WCHAR key[1024]) {
	ListNotes.clear();
	if (wcslen(key) == 0)
		return;
	for (int i = 0; i < NotesData.size(); i++) {
		if (wcsstr(NotesData[i]->NoteName, key)) {
			Note* x = new Note();
			wcscpy_s(x->TagName, NotesData[i]->TagName);
			wcscpy_s(x->NoteName, NotesData[i]->NoteName);
			wcscpy_s(x->NoteContent, NotesData[i]->NoteContent);
			ListNotes.push_back(x);
		}
	}
}

void ShowListAfterSearch() {
	ListView_DeleteAllItems(listView_ShowNotes);
	listShowNoteCount = 0;
	for (int i = 0; i < ListNotes.size(); i++) {
		LV_ITEM lv;
		lv.mask = LVIF_TEXT | LVIF_PARAM;
		lv.iItem = listShowNoteCount;
		lv.iSubItem = 0;
		lv.pszText = ListNotes[i]->TagName;
		ListView_InsertItem(listView_ShowNotes, &lv);
		
		lv.mask = LVIF_TEXT;
		lv.iSubItem = 1;
		lv.pszText = ListNotes[i]->NoteName;
		ListView_SetItem(listView_ShowNotes, &lv);

		listShowNoteCount++;
	}
}

int TotalNotes() {
	int count = 0;
	for (int i = 0; i < NotesData.size(); i++) {
		count++;
	}
	return count;
}

void sort(int *a, int n) {
	for (int i = 0; i < n-1; i++) {
		for (int j = i + 1; j < n; j++) {
			if (a[i] > a[j]) {
				int t = a[i];
				a[i] = a[j];
				a[j] = t;
			}
		}
	}
}

void CreateColor() {
	ListColor.push_back(RGB(255, 0, 0));
	ListColor.push_back(RGB(255, 128, 0));
	ListColor.push_back(RGB(255, 255, 0));
	ListColor.push_back(RGB(128, 255, 0));
	ListColor.push_back(RGB(0, 255, 0));
	ListColor.push_back(RGB(0, 255, 128));
	ListColor.push_back(RGB(0, 255, 255));
	ListColor.push_back(RGB(0, 128, 255));
	ListColor.push_back(RGB(0, 0, 255));
	ListColor.push_back(RGB(127, 0, 255));
	ListColor.push_back(RGB(255, 0, 255));
	ListColor.push_back(RGB(255, 0, 127));
}

void AddNotificationIcon(HWND hWnd) {
	ZeroMemory(&notifyIcon, sizeof(NOTIFYICONDATA));
	notifyIcon.cbSize = sizeof(NOTIFYICONDATA);
	notifyIcon.uID = IDI_IconNotification;
	notifyIcon.hBalloonIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_IconNotification));
	notifyIcon.hIcon = (HICON)LoadIcon(hInst, MAKEINTRESOURCE(IDI_IconNotification));
	notifyIcon.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	notifyIcon.hWnd = hWnd;
	notifyIcon.uCallbackMessage = WM_MYMESSAGE;
	wcscpy_s(notifyIcon.szTip, L"My Note");
	Shell_NotifyIcon(NIM_MODIFY, &notifyIcon);
	Shell_NotifyIcon(NIM_ADD, &notifyIcon);
}

LRESULT CALLBACK MyHookProc(int nCode, WPARAM wParam, LPARAM lParam) {
	if (nCode < 0) {
		return CallNextHookEx(hHook, nCode, wParam, lParam);
	}
	if ((GetAsyncKeyState(VK_LWIN) < 0) && (GetAsyncKeyState(VK_LCONTROL) < 0)) {
		if (isHiding) {
			ShowWindow(g_hWnd, SW_SHOWDEFAULT);
			isHiding = false;
		}
		else {
			isHiding = true;
			ShowWindow(g_hWnd, SW_MINIMIZE);
		}
	}
	return CallNextHookEx(hHook, nCode, wParam, lParam);
}

void doInstallHook(HWND hWnd) {
	if (hHook != NULL) {
		return;
	}
	hHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)MyHookProc, hinstLib, 0);
}

void doRemoveHook(HWND hWnd) {
	if (hHook == NULL)
		return;
	UnhookWindowsHookEx(hHook);
	hHook = NULL;
}
