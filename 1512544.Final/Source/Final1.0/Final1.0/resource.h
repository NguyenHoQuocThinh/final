//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Final1.0.rc
//
#define IDC_MYICON                      2
#define IDD_FINAL10_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_FINAL10                     107
#define IDI_SMALL                       108
#define IDC_FINAL10                     109
#define ID_LV_TAGS                      111
#define ID_LV_NOTENAME                  112
#define ID_LV_SHOWNOTES                 113
#define IDD_ADDTAG                      115
#define IDC_ADDTAG                      116
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDD_RENAMETAG                   133
#define IDD_VIEWSTATISTIC               135
#define IDI_ICON1                       137
#define IDI_IconNotification            137
#define IDC_NEWTAG                      1001
#define IDC_ADDTAG1                     1002
#define IDC_EDIT1                       1004
#define IDC_EDIT_RENAMETAG              1004
#define IDC_STATIC_RENAMETAG            1005
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
