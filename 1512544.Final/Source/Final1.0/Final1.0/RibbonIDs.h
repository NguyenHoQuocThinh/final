// *****************************************************************************
// * This is an automatically generated header file for UI Element definition  *
// * resource symbols and values. Please do not modify manually.               *
// *****************************************************************************

#pragma once

#define tabMain 2 
#define tabMain_LabelTitle_RESID 60001
#define groupFile 3 
#define groupFile_LabelTitle_RESID 60002
#define groupToolNote 4 
#define groupToolNote_LabelTitle_RESID 60003
#define groupToolTag 5 
#define groupToolTag_LabelTitle_RESID 60004
#define groupViewStatistic 6 
#define groupViewStatistic_LabelTitle_RESID 60005
#define ID_BTN_EXIT 7 
#define ID_BTN_EXIT_LabelTitle_RESID 60006
#define ID_BTN_EXIT_LargeImages_RESID 200
#define ID_BTN_SAVE 8 
#define ID_BTN_SAVE_LabelTitle_RESID 60007
#define ID_BTN_SAVE_LargeImages_RESID 60008
#define ID_BTN_OPEN 9 
#define ID_BTN_OPEN_LabelTitle_RESID 60009
#define ID_BTN_OPEN_LargeImages_RESID 60010
#define ID_BTN_ADDNOTE 10 
#define ID_BTN_ADDNOTE_LabelTitle_RESID 60011
#define ID_BTN_ADDNOTE_LargeImages_RESID 100
#define ID_BTN_CLEARNOTE 11 
#define ID_BTN_CLEARNOTE_LabelTitle_RESID 60012
#define ID_BTN_CLEARNOTE_LargeImages_RESID 101
#define ID_BTN_DELETENOTE 12 
#define ID_BTN_DELETENOTE_LabelTitle_RESID 60013
#define ID_BTN_DELETENOTE_LargeImages_RESID 102
#define ID_BTN_VIEWNOTE 13 
#define ID_BTN_VIEWNOTE_LabelTitle_RESID 60014
#define ID_BTN_VIEWNOTE_LargeImages_RESID 103
#define ID_BTN_ADDTAG 14 
#define ID_BTN_ADDTAG_LabelTitle_RESID 60015
#define ID_BTN_ADDTAG_LargeImages_RESID 300
#define ID_BTN_DELETETAG 15 
#define ID_BTN_DELETETAG_LabelTitle_RESID 60016
#define ID_BTN_DELETETAG_LargeImages_RESID 301
#define ID_BTN_RENAMETAG 16 
#define ID_BTN_RENAMETAG_LabelTitle_RESID 60017
#define ID_BTN_RENAMETAG_LargeImages_RESID 302
#define ID_BTN_VIEWTAG 17 
#define ID_BTN_VIEWTAG_LabelTitle_RESID 60018
#define ID_BTN_VIEWTAG_LargeImages_RESID 303
#define ID_BTN_VIEWSTATISTIC 18 
#define ID_BTN_VIEWSTATISTIC_LabelTitle_RESID 60019
#define ID_BTN_VIEWSTATISTIC_LargeImages_RESID 401
#define InternalCmd2_LabelTitle_RESID 60020
